#autostart
 10 REG 7,3
 20 ;****** Main menu ******
 30 CLS
 40 REG 127,0
 50 .SPUI menu -i "menu.ini"
 60 LET %m = % REG 127
 70 IF %m = 1 THEN PROC submenu("pawn", "The Pawn"): GOTO 30
 80 IF %m = 2 THEN PROC submenu("guild", "The Guild of Thieves"): GOTO 30
 90 IF %m = 3 THEN PROC submenu("jinxter", "Jinxter"): GOTO 30
100 IF %m = 4 THEN PROC submenu("corrupt", "Corruption"): GOTO 30
110 IF %m = 5 THEN PROC submenu("fish", "Fish!"): GOTO 30
120 IF %m = 6 THEN PROC submenu("myth", "Myth"): GOTO 30
130 IF %m = 7 THEN PROC submenu("wonder", "Wonderland"): GOTO 30
140 IF %m = 8 THEN .guide run.gde: GOTO 30
150 ERASE
160 ;****** Sub menu ******
170 DEFPROC submenu(d$, n$)
180   CLS
190   REG 127,0
200   CD d$
210   LET a$ = "menu -x 3 -y 9 -c 26 -r 3 -t """ + n$ + """ ""Run game in 256x192 mode"" ""Run game in 512x192 mode"" ""Back..."""
220   .$ SPUI a$
230   LET %s = % REG 127
240   IF %s = 1 THEN LET g$ = d$ + "_256.nex": .$ nexload g$
250   IF %s = 2 THEN LET g$ = d$ + "_512.nex": .$ nexload g$
260   CD ".."
270 ENDPROC
